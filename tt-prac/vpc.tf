provider "aws" {
       region = "us-east-1"
    }

# Creating VPC

    resource "aws_vpc" "main" {
       cidr_block       = var.vpc_cidr
       instance_tenancy = var.tenancy

       tags = {
         Name = "my-vpc"
        }
       }

# Creating Internet-Gateway(igw)

    resource "aws_internet_gateway" "main" {
        vpc_id = aws_vpc.main.id
       }

# Printing the ids VPC and IGW

      output "vpc_id" {
        value = "${aws_vpc.main.id}"
        }
       

      output "gateway_id" {
         value = "${aws_internet_gateway.main.id}"
      }
# Creating Subnet in Availability-zone= us-east-1a

   resource "aws_subnet" "subnet_1" {
       vpc_id            = aws_vpc.main.id
       availability_zone = var.availability_zone_1
       cidr_block        = var.subnet_cidr_1

           tags = {
          Name = "subnet-1"
          }
           }

# Creating subnet in Availability-zone= us-east-1b

  resource "aws_subnet" "subnet_2" {
      vpc_id            = aws_vpc.main.id
      availability_zone = var.availability_zone_2
      cidr_block        = var.subnet_cidr_2

            tags = {
          Name = "subnet-2"
          }
           }

# Printing id of Subnet-1 and Subnet-2

   output "subnet_id_1" {
         value = "${aws_subnet.subnet_1.id}"
      }

           output "subnet_id_2" {
         value = "${aws_subnet.subnet_2.id}"
      }
# Creating Route-Table for VPC

   resource "aws_route_table" "r" {
        vpc_id = aws_vpc.main.id

    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.main.id
        }

        tags = {
           Name = "route-table"
           }
     }

# Associating Subnet-1 with Route-Table

   resource "aws_route_table_association" "rt-1" {
        subnet_id      = aws_subnet.subnet_1.id
        route_table_id = aws_route_table.r.id
       }

# Associating Subnet-2 with Route-Table

   resource "aws_route_table_association" "rt-2" {
       subnet_id      = aws_subnet.subnet_2.id
       route_table_id = aws_route_table.r.id
       }

# Printing id of Route-table

    output "route_table_id" {
         value = "${aws_route_table.r.id}"
      }
# creating ec2-instance with public-ip and in az=us-east-1a and also with key 

   resource "aws_instance" "web" {
    ami                             = var.ami_id
    instance_type                   = var.instance_type
    subnet_id                       = aws_subnet.subnet_1.id
    key_name                        = aws_key_pair.newkey.key_name
    associate_public_ip_address     = true

  tags = {
    Name = "Instance-1"
    }
  }
   resource "aws_key_pair" "newkey" {
     public_key = file("./flip.pub")

   }
