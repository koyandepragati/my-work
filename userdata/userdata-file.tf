provider "aws" {
  region = "us-east-1"
  }
 
 resource "aws_key_pair" "deploy" {
  public_key = file("./public-key.pub")
 }

 resource "aws_instance" "user_data_input_file" {
  ami                         = var.ami_id
  instance_type               = var.instance_type
  key_name                    = aws_key_pair.deploy.key_name
  user_data                   = file("config.sh")
  associate_public_ip_address = true

  tags = {
    Name = "Ec2-User-data-with-file"
  }
}

