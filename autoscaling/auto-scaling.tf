provider "aws" {
  region = "us-east-1"
  }

  resource "aws_launch_configuration" "launch-configuration" {
      name          = "as-lc"
      image_id      = "ami-0323c3dd2da7fb37d"
      instance_type = "t2.micro"
   } 

  resource "aws_autoscaling_group" "autoscaling-group" {
      name                      = "as-lg"
      launch_configuration      = "as-lc"
      max_size                  = "4"
      min_size                  = "2"
      health_check_grace_period = "300"
      vpc_zone_identifier       = ["subnet-1a7f5524", "subnet-40c92e26"]
}
